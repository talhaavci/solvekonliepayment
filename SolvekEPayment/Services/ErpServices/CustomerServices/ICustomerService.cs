﻿using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Dtos.ErpDtos;
using SolvekEPayment.Models;
using SolvekEPayment.Models.ErpModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Services.ErpServices.CustomerServices
{
    public interface ICustomerService
    {
        PageViewModel<CustomerReceiptBalanceModelResponse> CustomerReceiptBalances(CustomerBalanceDto customerBalanceDto);
        PageViewModel<CustomerBalanceModelResponse> CustomerBalances(CustomerBalanceDto customerBalanceDto);
        CustomerBalanceTotalModelResponse CustomerBalanceTotal(int userId);
        InvoiceTitleModelResponse InvoiceTitle(InvoiceTitleDto ınvoiceTitleDto);
    }
}
