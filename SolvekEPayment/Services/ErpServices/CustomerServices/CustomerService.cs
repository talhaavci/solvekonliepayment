﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvekEPayment.Dtos.ErpDtos;
using SolvekEPayment.Models;
using SolvekEPayment.Models.ErpModels;
using SolvekEPayment.Repositories.ErpRepositories.CustomerRespositories;

namespace SolvekEPayment.Services.ErpServices.CustomerServices
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository CustomerRepository;
        public CustomerService(ICustomerRepository customerRepository)
        {
            CustomerRepository = customerRepository;
        }
        public PageViewModel<CustomerReceiptBalanceModelResponse> CustomerReceiptBalances(CustomerBalanceDto customerBalanceDto)
        {
            return CustomerRepository.CustomerReceiptBalances(customerBalanceDto);
        }

        public CustomerBalanceTotalModelResponse CustomerBalanceTotal(int userId)
        {
            return CustomerRepository.CustomerBalanceTotal(userId);
        }

        public InvoiceTitleModelResponse InvoiceTitle(InvoiceTitleDto ınvoiceTitleDto)
        {
            return CustomerRepository.InvoiceTitle(ınvoiceTitleDto);
        }

        public PageViewModel<CustomerBalanceModelResponse> CustomerBalances(CustomerBalanceDto customerBalanceDto)
        {
            customerBalanceDto.startDate = customerBalanceDto.startDate == null ? Convert.ToDateTime(DateTime.Now.ToShortDateString()) : customerBalanceDto.startDate;
            customerBalanceDto.endDate = customerBalanceDto.endDate == null ? Convert.ToDateTime(DateTime.Now.ToShortDateString()) : customerBalanceDto.endDate;
            return CustomerRepository.CustomerBalances(customerBalanceDto);
        }
    }
}
