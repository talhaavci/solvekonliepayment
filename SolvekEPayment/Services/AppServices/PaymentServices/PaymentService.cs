﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models;
using SolvekEPayment.Repositories.AppRepositories.PaymentRepositories;

namespace SolvekEPayment.Services.AppServices.PaymentServices
{
    public class PaymentService : IPaymentService
    {
        private readonly IPaymentRepository PaymentRepository;
        public PaymentService(IPaymentRepository paymentRepository)
        {
            PaymentRepository = paymentRepository;
        }

        public MessageModelResponse PaymentSave(PaymentDto paymentDto)
        {
            return PaymentRepository.PaymentSave(paymentDto);
        }
    }
}
