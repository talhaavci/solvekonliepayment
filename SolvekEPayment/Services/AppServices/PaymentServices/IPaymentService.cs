﻿using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Services.AppServices.PaymentServices
{
    public interface IPaymentService
    {
        MessageModelResponse PaymentSave(PaymentDto paymentDto);
    }
}
