﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models;
using SolvekEPayment.Models.AppModels;
using SolvekEPayment.Repositories.AppRepositories.ForgotRepositories;

namespace SolvekEPayment.Services.AppServices.ForgotServices
{
    public class ForgotService : IForgotService
    {
        private readonly IForgotRepository ForgotRepository;
        public ForgotService(IForgotRepository forgotRepository)
        {
            ForgotRepository = forgotRepository;
        }
        public ForgotModelResponse Forgot(ForgotDto forgotDto)
        {
            return ForgotRepository.Forgot(forgotDto);
        }
    }
}
