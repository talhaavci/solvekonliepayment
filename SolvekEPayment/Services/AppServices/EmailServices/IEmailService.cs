﻿using SolvekEPayment.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Services.AppServices.EmailServices
{
    public interface IEmailService
    {
        EmailModelResponse Email();
    }
}
