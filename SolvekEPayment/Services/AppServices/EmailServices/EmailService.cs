﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvekEPayment.Models.AppModels;
using SolvekEPayment.Repositories.AppRepositories.EmailRespositories;

namespace SolvekEPayment.Services.AppServices.EmailServices
{
    public class EmailService : IEmailService
    {
        private readonly IEmailRepository EmailRepository;
        public EmailService(IEmailRepository emailRepository)
        {
            EmailRepository = emailRepository;
        }

        public EmailModelResponse Email()
        {
            return EmailRepository.Email();
        }
    }
}
