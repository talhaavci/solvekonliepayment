﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models;
using SolvekEPayment.Models.AppModels;
using SolvekEPayment.Repositories.AppRepositories.LoginRepositories;

namespace SolvekEPayment.Services.AppServices.LoginServices
{
    public class LoginService : ILoginService
    {
        private readonly ILoginRepository LoginRepository;
        public LoginService(ILoginRepository loginRepository)
        {
            LoginRepository = loginRepository;
        }
        public LoginModelResponse Login(LoginDto loginDto)
        {
            return LoginRepository.Login(loginDto);
        }
        public MessageModelResponse ChangePassword(ChangePasswordDto changePasswordDto)
        {
            if(string.IsNullOrEmpty(changePasswordDto.password) || string.IsNullOrEmpty(changePasswordDto.passwordAgain)
                || string.IsNullOrEmpty(changePasswordDto.userName))
                return new MessageModelResponse { result = false, message = MessageHelper.NOT_EMPTY };
            if (changePasswordDto.password.Length < 6)
                return new MessageModelResponse { result = false, message = MessageHelper.CHANGE_PASSWORD_LENGTH };
            if (!changePasswordDto.password.Equals(changePasswordDto.passwordAgain))
                return new MessageModelResponse { result = false, message = MessageHelper.CHANGE_PASSWORD_MISMATCH };
            return LoginRepository.ChangePassword(changePasswordDto);
        }
    }
}
