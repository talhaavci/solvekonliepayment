﻿using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models;
using SolvekEPayment.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Services.AppServices.LoginServices
{
    public interface ILoginService
    {
        LoginModelResponse Login(LoginDto loginDto);
        MessageModelResponse ChangePassword(ChangePasswordDto changePasswordDto);
    }
}
