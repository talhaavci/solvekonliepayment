﻿using SolvekEPayment.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Services.AppServices.InstallmentServices
{
    public interface IInstallmentService
    {
        Task<InstallmentModelResponse> Installment(int userId);
    }
}
