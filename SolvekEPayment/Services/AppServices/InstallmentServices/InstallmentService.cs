﻿using SolvekEPayment.Models.AppModels;
using SolvekEPayment.Repositories.AppRepositories.InstallmentRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Services.AppServices.InstallmentServices
{
    public class InstallmentService : IInstallmentService
    {
        private readonly IInstallmentRepository InstallmentRepository;
        public InstallmentService(IInstallmentRepository ınstallmentRepository)
        {
            InstallmentRepository = ınstallmentRepository;
        }

        public Task<InstallmentModelResponse> Installment(int userId)
        {
            return InstallmentRepository.Installment(userId);
        }
    }
}
