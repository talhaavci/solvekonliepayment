﻿using Microsoft.AspNetCore.Mvc;
using SolvekEPayment.Dtos.ErpDtos;
using SolvekEPayment.Services.ErpServices.CustomerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.ViewComponents
{
    public class InvoiceTitleComponent : ViewComponent
    {
        private readonly ICustomerService CustomerService;
        public InvoiceTitleComponent(ICustomerService customerService)
        {
            CustomerService = customerService;
        }
        public async Task<IViewComponentResult> InvokeAsync(int id)
        {
            return View(CustomerService.InvoiceTitle(new InvoiceTitleDto
            {
                id = id,
                userId = Convert.ToInt32(Request.Cookies["userId"])
            }));
        }
    }
}
