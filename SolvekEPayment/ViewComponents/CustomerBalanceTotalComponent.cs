﻿using Microsoft.AspNetCore.Mvc;
using SolvekEPayment.Services.ErpServices.CustomerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.ViewComponents
{
    public class CustomerBalanceTotalComponent : ViewComponent
    {
        private readonly ICustomerService CustomerService;
        public CustomerBalanceTotalComponent(ICustomerService customerService)
        {
            CustomerService = customerService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View(CustomerService.CustomerBalanceTotal(Convert.ToInt32(Request.Cookies["userId"])));
        }
    }
}
