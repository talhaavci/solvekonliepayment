﻿using Microsoft.AspNetCore.Mvc;
using SolvekEPayment.Models.AppModels;
using SolvekEPayment.Services.AppServices.InstallmentServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.ViewComponents
{
    public class InstallmentComponent : ViewComponent
    {
        private readonly IInstallmentService InstallmentService;
        public InstallmentComponent(IInstallmentService ınstallmentService)
        {
            InstallmentService = ınstallmentService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            var installments = new List<InstallmentModelResponse>();
            var result = await InstallmentService.Installment(Convert.ToInt32(Request.Cookies["userId"]));
            if (result == null)
                installments.Add(new InstallmentModelResponse { installment = 1 });

            for (int i = 1; i <= result.installment; i++)
            {
                installments.Add(new InstallmentModelResponse { installment = i });
            }
            return View(installments);
        }
    }
}
