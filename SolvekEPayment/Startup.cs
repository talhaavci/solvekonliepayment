﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SolvekEPayment.Banks;
using SolvekEPayment.Banks.Akbank;
using SolvekEPayment.Banks.Halkbank;
using SolvekEPayment.Banks.Isbank;
using SolvekEPayment.Banks.Teb;
using SolvekEPayment.Banks.Vakifbank;
using SolvekEPayment.Repositories.AppRepositories.EmailRespositories;
using SolvekEPayment.Repositories.AppRepositories.ForgotRepositories;
using SolvekEPayment.Repositories.AppRepositories.InstallmentRepositories;
using SolvekEPayment.Repositories.AppRepositories.LoginRepositories;
using SolvekEPayment.Repositories.AppRepositories.PaymentRepositories;
using SolvekEPayment.Repositories.ErpRepositories.CustomerRespositories;
using SolvekEPayment.SendInfos;
using SolvekEPayment.Services.AppServices.EmailServices;
using SolvekEPayment.Services.AppServices.ForgotServices;
using SolvekEPayment.Services.AppServices.InstallmentServices;
using SolvekEPayment.Services.AppServices.LoginServices;
using SolvekEPayment.Services.AppServices.PaymentServices;
using SolvekEPayment.Services.ErpServices.CustomerServices;
using SolvekEPayment.SolvekLog;

namespace SolvekEPayment
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddMvc();
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.LoginPath = "/Login/Login/";
                        options.Cookie.Name = "solvekonlinepaymentcookie";
                        options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                        options.SlidingExpiration = true;
                    });

            services.Configure<CookieTempDataProviderOptions>(options => {
                options.Cookie.IsEssential = true;
            });

            //services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme).AddCookie();
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver
                    = new Newtonsoft.Json.Serialization.DefaultContractResolver();
                
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<ILoginRepository, LoginRepository>();
            services.AddScoped<ILoginService, LoginService>();

            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ICustomerService, CustomerService>();

            services.AddScoped<IForgotRepository, ForgotRepository>();
            services.AddScoped<IForgotService, ForgotService>();

            services.AddScoped<IEmailRepository, EmailRepository>();
            services.AddScoped<IEmailService, EmailService>();

            services.AddScoped<ISendInfo, SendEmail>();
            services.AddScoped<ISendEmailHelper, SendEmailHelper>();

            services.AddScoped<IPaymentRequest, PaymentRequest>();
            //services.AddScoped<IPaymentStart, PaymentStart>();

            services.AddScoped<PaymentStart>()
                  .AddScoped<IPaymentStart, PaymentStart>(s => s.GetService<PaymentStart>());

            services.AddScoped<HalkbankPaymentStart>()
                  .AddScoped<IPaymentStart, HalkbankPaymentStart>(s => s.GetService<HalkbankPaymentStart>());

            services.AddScoped<TebPaymentStart>()
                 .AddScoped<IPaymentStart, TebPaymentStart>(s => s.GetService<TebPaymentStart>());

            services.AddScoped<IsbankPaymentStart>()
                 .AddScoped<IPaymentStart, IsbankPaymentStart>(s => s.GetService<IsbankPaymentStart>());

            services.AddScoped<VakifbankPaymentStart>()
                 .AddScoped<IPaymentStart, VakifbankPaymentStart>(s => s.GetService<VakifbankPaymentStart>());

            services.AddScoped<IPaymentRepository, PaymentRepository>();
            services.AddScoped<IPaymentService, PaymentService>();

            services.AddScoped<IPaymentInitControl, PaymentStartControl>();

            services.AddScoped<ILog, Log>();

            services.AddScoped<IInstallmentRepository, InstallmentRepository>();
            services.AddScoped<IInstallmentService, InstallmentService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error/Error");
                app.UseHsts();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
