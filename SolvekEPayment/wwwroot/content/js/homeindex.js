﻿$(document).ready(function () {
    $('#pan').mask('0000 0000 0000 0000', {
        placeholder: '**** **** **** ****'
    });
    $('#expire').mask('00/00', {
        placeholder: 'MM/YY'
    });
    $('#cv').mask('000', {
        placeholder: '***'
    });
    $("#paymentTotal").inputmask("currency", {
        radixPoint: ",",
        groupSeparator: ".",
        digits: 2,
        autoGroup: true,
        rightAlign: false,
        prefix: ""
    });
});

$("#pan").on('change keydown paste input', function () {
    var panText = $("#pan").val();
    if (panText.startsWith("4")) {
        document.getElementById("master").style.display = "none";
        document.getElementById("visa").style.display = "block";
    } else if (panText.startsWith("5")) {
        document.getElementById("master").style.display = "block";
        document.getElementById("visa").style.display = "none";
    } else {
        document.getElementById("master").style.display = "none";
        document.getElementById("visa").style.display = "none";
    }
});

//function select_onchange_cardid(divId, element) {
    //var value = $("#bankId").val();
    //if (element.value == -1 || value == -1) {
    //    document.getElementById(divId).style.display = 'none';
    //    $("#installment").html('');
    //} else {
    //    document.getElementById(divId).style.display = 'block';
    //    $("#installment").html('');
    //    if ((element.value == 1 && value == 1) || (element.value == 5 && value == 2)
    //        || (element.value == 2 && value == 3) || (element.value == 4 && value == 4)
    //        || (element.value == 6 && value == 5)) {
    //        $("#installment").html('<option value="-1">-- Seçiniz --</option> <option value="1">1</option>' +
    //            '<option value="2">2</option>' +
    //            '<option value="3">3</option>' +
    //            '<option value="4">4</option>' +
    //            '<option value="5">5</option>' +
    //            '<option value="6">6</option>');
    //    }
    //    else {
    //        $("#installment").html('<option value="-1">-- Seçiniz --</option> <option value="1">1</option>');
    //    }
    //}
//}

//function select_onchange_bankid(divId, element) {
    //var value = $("#cardId").val();
    //if (element.value == -1 || value == -1) {
    //    document.getElementById(divId).style.display = 'none';
    //    $("#installment").html('');
    //} else {
    //    document.getElementById(divId).style.display = 'block';
    //    $("#installment").html('');
    //    if ((element.value == 1 && value == 1) || (element.value == 2 && value == 5)
    //        || (element.value == 3 && value == 2) || (element.value == 4 && value == 4)
    //        || (element.value == 5 && value == 6)) {
    //        $("#installment").html('<option value="-1">-- Seçiniz --</option> <option value="1">1</option>' +
    //            '<option value="2">2</option>' +
    //            '<option value="3">3</option>' +
    //            '<option value="4">4</option>' +
    //            '<option value="5">5</option>' +
    //            '<option value="6">6</option>');
    //    }
    //    else {
    //        $("#installment").html('<option value="-1">-- Seçiniz --</option> <option value="1">1</option>');
    //    }
    //}

//}