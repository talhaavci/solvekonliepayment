﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SolvekEPayment.Banks;
using SolvekEPayment.Banks.Akbank;
using SolvekEPayment.Banks.Halkbank;
using SolvekEPayment.Banks.Isbank;
using SolvekEPayment.Banks.Teb;
using SolvekEPayment.Banks.Vakifbank;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Dtos.ErpDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models;
using SolvekEPayment.Services.AppServices.PaymentServices;
using SolvekEPayment.Services.ErpServices.CustomerServices;
using SolvekEPayment.SolvekLog;

namespace SolvekEPayment.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private readonly ICustomerService CustomerService;
        private readonly IHostingEnvironment HostingEnvironment;
        private readonly IPaymentInitControl PaymentInitControl;
        private readonly IPaymentRequest PaymentRequest;
        private readonly IPaymentStart PaymentStart, PaymentStartHalkbank, PaymentStartTeb, PaymentStartIsbank, PaymentStartVakifbank;
        private readonly ILog Log;
        private readonly IPaymentService PaymentService;
        private string vakifTerminalNo = "VP294104";
        private string vakifTransactionType = "Sale";
        public HomeController(ICustomerService customerService, IHostingEnvironment hostingEnvironment, IPaymentRequest paymentRequest,
            IPaymentInitControl paymentInitControl, PaymentStart paymentStart, HalkbankPaymentStart paymentStartHalkbank,
            IsbankPaymentStart paymentStartIsbank, VakifbankPaymentStart paymentStartVakifbank,
            TebPaymentStart paymentStartTeb, ILog log, IPaymentService paymentService)
        {
            CustomerService = customerService;
            PaymentRequest = paymentRequest;
            HostingEnvironment = hostingEnvironment;
            PaymentInitControl = paymentInitControl;
            PaymentStart = paymentStart;
            PaymentStartHalkbank = paymentStartHalkbank;
            PaymentStartTeb = paymentStartTeb;
            PaymentStartIsbank = paymentStartIsbank;
            PaymentStartVakifbank = paymentStartVakifbank;
            Log = log;
            PaymentService = paymentService;
        }

        public IActionResult Index()
        {
            TempData["result"] = false;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Index(PaymentStartDto paymentStartDto)
        {
            if (!ModelState.IsValid)
                return View();

            paymentStartDto.userId = Convert.ToInt32(Request.Cookies["userId"]);
            paymentStartDto.paymentType = PaymentTypeHelper.BALANCE;
            paymentStartDto.path = HostingEnvironment.WebRootPath;
            var controlModel = PaymentInitControl.control(paymentStartDto);
            MessageModelResponse result = null;
            if (controlModel.result)
            {
                paymentStartDto.pan = paymentStartDto.pan.Trim().Replace(" ", "");
                paymentStartDto.documentNumber = "";

                switch (paymentStartDto.bankId)
                {
                    case BankIdHelper.AKBANK:
                        result = PaymentStart.paymentStart(paymentStartDto);
                        break;
                    case BankIdHelper.HALKBANK:
                        result = PaymentStartHalkbank.paymentStart(paymentStartDto);
                        break;
                    case BankIdHelper.TEB:
                        result = PaymentStartTeb.paymentStart(paymentStartDto);
                        break;
                    case BankIdHelper.ISBANK:
                        result = PaymentStartIsbank.paymentStart(paymentStartDto);
                        break;
                    case BankIdHelper.VAKIFBANK:
                        result = PaymentStartVakifbank.paymentStart(paymentStartDto);
                        break;
                    default:
                        break;
                }

                if (result.result)
                {
                    TempData["result"] = false;
                    //Log.log($"{paymentStartDto.path}\\logbank\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{paymentStartDto.userId}.xml", result.message);
                    byte[] bytes = Encoding.ASCII.GetBytes(result.message);
                    HttpContext.Response.Body.Write(bytes);
                }
                else
                {
                    TempData["result"] = true;
                    TempData["message"] = result.message;
                }
            }
            else
            {
                TempData["result"] = true;
                TempData["message"] = controlModel.message;
            }
            return View();
        }

        [AllowAnonymous]
        public IActionResult OkUrl([FromQuery]string bType)
        {
            var types = bType.Split('_');
            if (types[0] == "other")
            {
                List<KeyValuePair<String, String>> postParams = new List<KeyValuePair<String, String>>();
                foreach (string key in Request.Form.Keys)
                {
                    KeyValuePair<String, String> newKeyValuePair = new KeyValuePair<String, String>(key, Request.Form[key]);
                    postParams.Add(newKeyValuePair);
                }

                String hashVal = "";
                String paramskeys = "";

                postParams.Sort(
                delegate (KeyValuePair<string, string> firstPair,
                KeyValuePair<string, string> nextPair)
                {
                    return firstPair.Key.ToLower(new System.Globalization.CultureInfo("en-US", false)).CompareTo(nextPair.Key.ToLower(new System.Globalization.CultureInfo("en-US", false)));
                }
                );

                foreach (KeyValuePair<String, String> pair in postParams)
                {
                    String escapedValue = pair.Value.Replace("\\", "\\\\").Replace("|", "\\|");
                    String lowerValue = pair.Key.ToLower(new System.Globalization.CultureInfo("en-US", false));
                    if (!"encoding".Equals(lowerValue) && !"hash".Equals(lowerValue) && !"countdown".Equals(lowerValue))
                    {

                        hashVal += escapedValue + "|";
                        paramskeys += lowerValue + "|";
                    }
                }
                hashVal += AkbankHelper.storeKey;

                System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
                byte[] hashbytes = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(hashVal);
                byte[] inputbytes = sha.ComputeHash(hashbytes);
                String actualHash = System.Convert.ToBase64String(inputbytes);

                string HASH = Request.Form["HASH"];

                string mdstatus = Request.Form["mdStatus"];
                //string hashparams = Request.Form["HASHPARAMS"];
                //string hashparamsval = Request.Form["HASHPARAMSVAL"];

                /*string paramsval = "";
                int index1 = 0, index2 = 0;

                if (hashparams != null)
                {
                    do
                    {
                        index2 = hashparams.IndexOf(":", index1);
                        string val1 = Request.Form[hashparams.Substring(index1, index2 - index1)];
                        string val = val1 == null ? "" : val1;
                        paramsval += val;
                        index1 = index2 + 1;
                    }
                    while (index1 < hashparams.Length);

                    string hashval = paramsval + AkbankHelper.storeKey;              //Store key is being added to hash value
                    string hashparam = Request.Form["HASH"];

                    System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                    byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashval);
                    byte[] inputbytes = sha.ComputeHash(hashbytes);

                    string hash = Convert.ToBase64String(inputbytes);   //Hash value used for validation*/

                    if (!actualHash.Equals(HASH)) //hash generated in this page, hash returned and hash generated from parsed parameters should be same
                    {
                        try
                        {
                            TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                            {
                                message = MessageHelper.PAYMENT_HASH_ERROR,
                                oid = Request.Form["oid"],
                                userId = Convert.ToInt32(Request.Form["firmaadi"]),
                                transId = Request.Form["TransId"],
                                amount = Request.Form["amount"],
                                paymentType = PaymentTypeHelper.BALANCE
                            });
                        }
                        catch (Exception ex)
                        {

                        }
                        return RedirectToAction("PaymentFail", "Home");
                    }
                /*}
                else
                {
                    try
                    {
                        TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                        {
                            message = MessageHelper.PAYMENT_HASH_EMPTY_ERROR,
                            oid = Request.Form["oid"],
                            userId = Convert.ToInt32(Request.Form["firmaadi"]),
                            transId = Request.Form["TransId"],
                            amount = Request.Form["amount"],
                            paymentType = PaymentTypeHelper.BALANCE
                        });
                    }
                    catch (Exception ex)
                    {

                    }
                    return RedirectToAction("PaymentFail", "Home");
                }*/

                if (mdstatus.Equals("1") || mdstatus.Equals("2") || mdstatus.Equals("3") || mdstatus.Equals("4"))
                {
                    try
                    {
                        string transId = Request.Form["TransId"];
                        string errMsg = Request.Form["ErrMsg"];
                        string installment = Request.Form["taksit"];
                        installment = string.IsNullOrEmpty(installment) ? "1" : installment;
                        if (Request.Form["Response"] == "Approved")
                        {
                            var dto = new PaymentDto
                            {
                                documentId = 0,
                                documentNumber = "Bakiye",
                                paymentDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                                paymentType = PaymentTypeHelper.BALANCE,
                                transId = transId,
                                installment = installment,
                                userId = Convert.ToInt32(Request.Form["firmaadi"]),
                                bankId = Convert.ToInt32(Request.Form["Fpostakodu"]),
                                cardId = Convert.ToInt32(Request.Form["Fulkekodu"]),
                                path = $"{HostingEnvironment.WebRootPath}\\logorder\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{Request.Form["firmaadi"]}.xml",
                                paymentTotal = Convert.ToDouble(Request.Form["amount"].ToString().Replace(".", ",")),
                                oid = Request.Form["oid"],
                                message = MessageHelper.PAYMENT_SAVE_SUCCESS,
                            };
                            var model = PaymentService.PaymentSave(dto);
                            TempData["messageModel"] = JsonConvert.SerializeObject(dto);
                            return RedirectToAction("PaymentComplete", "Home");
                        }
                        else
                        {
                            try
                            {
                                TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                                {
                                    message = errMsg,
                                    oid = Request.Form["oid"],
                                    userId = Convert.ToInt32(Request.Form["firmaadi"]),
                                    transId = Request.Form["TransId"],
                                    amount = Request.Form["amount"],
                                    paymentType = PaymentTypeHelper.BALANCE
                                });
                            }
                            catch (Exception ex)
                            {

                            }
                            return RedirectToAction("PaymentFail", "Home");
                        }
                    }
                    catch (Exception ex)
                    {
                        try
                        {
                            JsonConvert.SerializeObject(TempData["messageModel"] = new PaymentDto
                            {
                                message = ex.Message,
                                oid = Request.Form["oid"],
                                userId = Convert.ToInt32(Request.Form["firmaadi"]),
                                transId = Request.Form["TransId"],
                                amount = Request.Form["amount"],
                                paymentType = PaymentTypeHelper.BALANCE
                            });
                        }
                        catch (Exception ex1)
                        {

                        }
                        return RedirectToAction("PaymentFail", "Home");
                    }
                }
                else
                {
                    try
                    {
                        TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                        {
                            message = MessageHelper.PAYMENT_3D_ERROR,
                            oid = Request.Form["oid"],
                            userId = Convert.ToInt32(Request.Form["firmaadi"]),
                            transId = Request.Form["TransId"],
                            amount = Request.Form["amount"],
                            paymentType = PaymentTypeHelper.BALANCE
                        });
                    }
                    catch (Exception ex1)
                    {

                    }
                    return RedirectToAction("PaymentFail", "Home");
                }
            }
            else if (types[0] == "vakifbank")
            {
                string status = Request.Form["Status"];
                string merchantId = Request.Form["MerchantId"];
                string verifyEnrollmentRequestId = Request.Form["VerifyEnrollmentRequestId"];
                string xid = Request.Form["Xid"];
                string purchAmount = Request.Form["PurchAmount"];
                string sessionInfo = Request.Form["SessionInfo"];
                string purchCurrency = Request.Form["PurchCurrency"];
                string pan = Request.Form["Pan"];
                string expiryDate = Request.Form["Expiry"];
                string eci = Request.Form["Eci"];
                string cavv = Request.Form["Cavv"];
                string installmentCount = Request.Form["InstallmentCount"];

                string xmlStr = "prmstr=<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                                $@"<VposRequest><MerchantId>{merchantId}</MerchantId><Password>{VakifbankHelper.password}</Password><TerminalNo>{vakifTerminalNo}</TerminalNo><TransactionType>{vakifTransactionType}</TransactionType><TransactionId>{verifyEnrollmentRequestId}</TransactionId>{(string.IsNullOrEmpty(installmentCount) ? "" : $"<NumberOfInstallments>{installmentCount}</NumberOfInstallments>")}<Pan>{pan}</Pan><Cvv>{sessionInfo}</Cvv><Expiry>20{expiryDate}</Expiry><ECI>{eci}</ECI><CAVV>{cavv}</CAVV><CurrencyAmount>{purchAmount.Insert(purchAmount.Length - 2, ".")}</CurrencyAmount><CurrencyCode>{purchCurrency}</CurrencyCode><MpiTransactionId>{verifyEnrollmentRequestId}</MpiTransactionId><ClientIp>31.169.93.14</ClientIp><OrderId></OrderId><OrderDescription></OrderDescription><TransactionDeviceSource>0</TransactionDeviceSource></VposRequest>";

                string postUrl = "https://onlineodeme.vakifbank.com.tr:4443/VposService/v3/Vposreq.aspx";
                var resultRequest = PaymentRequest.sendRequest(new PostBankDto
                {
                    path = HostingEnvironment.WebRootPath,
                    postData = xmlStr,
                    postUrl = postUrl
                });

                if (!resultRequest.result || string.IsNullOrEmpty(resultRequest.message))
                {
                    try
                    {
                        TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                        {
                            message = resultRequest.message,
                            oid = verifyEnrollmentRequestId,
                            userId = Convert.ToInt32(types[1]),
                            transId = "",
                            amount = purchAmount,
                            paymentType = PaymentTypeHelper.BALANCE
                        });
                    }
                    catch (Exception ex)
                    {

                    }
                    return RedirectToAction("PaymentFail", "Home");
                }

                if (!resultRequest.message.Contains("<ResultCode>0000</ResultCode>"))
                {
                    try
                    {
                        Log.log($"{HostingEnvironment.WebRootPath}\\logbank\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{types[1]}.xml", resultRequest.message);
                        TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                        {
                            message = MessageHelper.SORRY_PAYMENTSTART_MESSAGE_ERROR,
                            oid = verifyEnrollmentRequestId,
                            userId = Convert.ToInt32(types[1]),
                            transId = "",
                            amount = purchAmount,
                            paymentType = PaymentTypeHelper.BALANCE
                        });
                    }
                    catch (Exception ex)
                    {

                    }
                    return RedirectToAction("PaymentFail", "Home");
                }

                purchAmount = purchAmount.Insert(purchAmount.Length - 2, ",");
                var dto = new PaymentDto
                {
                    documentId = 0,
                    documentNumber = "Bakiye",
                    paymentDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                    paymentType = PaymentTypeHelper.BALANCE,
                    transId = "",
                    installment = string.IsNullOrEmpty(installmentCount) ? "1" : installmentCount,
                    userId = Convert.ToInt32(types[1]),
                    bankId = Convert.ToInt32(types[2]),
                    cardId = Convert.ToInt32(types[3]),
                    path = $"{HostingEnvironment.WebRootPath}\\logorder\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{types[1]}.xml",
                    paymentTotal = Convert.ToDouble(purchAmount),
                    oid = verifyEnrollmentRequestId,
                    message = MessageHelper.PAYMENT_SAVE_SUCCESS,
                };
                var model = PaymentService.PaymentSave(dto);
                TempData["messageModel"] = JsonConvert.SerializeObject(dto);
                return RedirectToAction("PaymentComplete", "Home");
            }
            else
            {
                try
                {
                    TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                    {
                        message = MessageHelper.SORRY_MESSAGE_ERROR,
                        oid = "",
                        userId = Convert.ToInt32(types[1]),
                        transId = "",
                        amount = "",
                        paymentType = PaymentTypeHelper.BALANCE
                    });
                }
                catch (Exception ex1)
                {

                }
                return RedirectToAction("PaymentFail", "Home");
            }
        }

        [AllowAnonymous]
        public IActionResult FailUrl([FromQuery]string bType)
        {
            var types = bType.Split('_');
            if(types[0] == "other")
            {
                try
                {
                    string errMsg = Request.Form["ErrMsg"];
                    TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                    {
                        message = string.IsNullOrEmpty(errMsg) ? MessageHelper.SORRY_PAYMENTSTART_MESSAGE_ERROR : errMsg,
                        oid = Request.Form["oid"],
                        userId = Convert.ToInt32(Request.Form["firmaadi"]),
                        transId = Request.Form["TransId"],
                        amount = Request.Form["amount"],
                        paymentType = PaymentTypeHelper.BALANCE
                    });
                }
                catch (Exception ex1)
                {

                }
            }
            else if(types[0] == "vakifbank")
            {
                TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                {
                    message = MessageHelper.SORRY_PAYMENTSTART_MESSAGE_ERROR,
                    oid = Request.Form["VerifyEnrollmentRequestId"],
                    userId = Convert.ToInt32(types[1]),
                    transId = "",
                    amount = Request.Form["PurchAmount"],
                    paymentType = PaymentTypeHelper.BALANCE
                });
            }
            else
            {
                TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                {
                    message = MessageHelper.SORRY_PAYMENTSTART_MESSAGE_ERROR,
                    oid = "",
                    userId = Convert.ToInt32(types[1]),
                    transId = "unknown",
                    amount = "unknown",
                    paymentType = PaymentTypeHelper.BALANCE
                });
            }
            return RedirectToAction("PaymentFail", "Home");
        }

        [AllowAnonymous]
        public IActionResult PaymentFail()
        {
            PaymentDto paymentDto = null;
            if (TempData["messageModel"] != null)
            {
                paymentDto = JsonConvert.DeserializeObject<PaymentDto>(TempData["messageModel"].ToString());
                Log.log($"{HostingEnvironment.WebRootPath}\\logbank\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{paymentDto.userId}.xml", paymentDto);
            }

            return View(paymentDto);
        }

        [AllowAnonymous]
        public IActionResult PaymentComplete()
        {
            PaymentDto paymentDto = null;
            if (TempData["messageModel"] != null)
            {
                paymentDto = JsonConvert.DeserializeObject<PaymentDto>(TempData["messageModel"].ToString());
            }
            return View(paymentDto);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Contract()
        {
            return View();
        }

        public IActionResult CustomerBalance()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CustomerBalanceList(CustomerBalanceDto customerBalanceDto)
        {
            customerBalanceDto.userId = Convert.ToInt32(Request.Cookies["userId"]);
            return Json(CustomerService.CustomerBalances(customerBalanceDto));
        }

        public IActionResult CustomerReceiptDetail()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CustomerReceiptDetailList(CustomerBalanceDto customerBalanceDto)
        {
            customerBalanceDto.userId = Convert.ToInt32(Request.Cookies["userId"]);
            return Json(CustomerService.CustomerReceiptBalances(customerBalanceDto));
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Deneme()
        {
            return View();
        }
    }
}
