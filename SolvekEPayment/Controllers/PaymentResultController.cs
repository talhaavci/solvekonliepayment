﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SolvekEPayment.Banks.Akbank;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models;
using SolvekEPayment.Services.AppServices.PaymentServices;
using SolvekEPayment.SolvekLog;

namespace SolvekEPayment.Controllers
{
    [Authorize]
    public class PaymentResultController : Controller
    {
        private readonly ILog Log;
        private readonly IPaymentService PaymentService;
        private readonly IHostingEnvironment HostingEnvironment;
        public PaymentResultController(ILog log, IPaymentService paymentService, IHostingEnvironment hostingEnvironment)
        {
            Log = log;
            PaymentService = paymentService;
            HostingEnvironment = hostingEnvironment;
        }

        /*public IActionResult OkUrl()
        {
            string mdstatus = Request.Form["mdStatus"];
            string hashparams = Request.Form["HASHPARAMS"];
            string hashparamsval = Request.Form["HASHPARAMSVAL"];

            string paramsval = "";
            int index1 = 0, index2 = 0;

            if (hashparams != null)
            {
                do
                {
                    index2 = hashparams.IndexOf(":", index1);
                    string val1 = Request.Form[hashparams.Substring(index1, index2 - index1)];
                    string val = val1 == null ? "" : val1;
                    paramsval += val;
                    index1 = index2 + 1;
                }
                while (index1 < hashparams.Length);

                string hashval = paramsval + AkbankHelper.storeKey;              //Store key is being added to hash value
                string hashparam = Request.Form["HASH"];

                System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashval);
                byte[] inputbytes = sha.ComputeHash(hashbytes);

                string hash = Convert.ToBase64String(inputbytes);   //Hash value used for validation

                if (!paramsval.Equals(hashparamsval) || !hash.Equals(hashparam)) //hash generated in this page, hash returned and hash generated from parsed parameters should be same
                {
                    try
                    {
                        TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                        {
                            message = MessageHelper.PAYMENT_HASH_ERROR,
                            oid = Request.Form["oid"],
                            userId = Convert.ToInt32(Request.Form["firmaadi"]),
                            transId = Request.Form["TransId"],
                            amount = Request.Form["amount"],
                            paymentType = PaymentTypeHelper.BALANCE
                        });
                    }
                    catch (Exception ex)
                    {

                    }
                    return RedirectToAction("PaymentFail", "PaymentResult");
                }
            }
            else
            {
                try
                {
                    TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                    {
                        message = MessageHelper.PAYMENT_HASH_EMPTY_ERROR,
                        oid = Request.Form["oid"],
                        userId = Convert.ToInt32(Request.Form["firmaadi"]),
                        transId = Request.Form["TransId"],
                        amount = Request.Form["amount"],
                        paymentType = PaymentTypeHelper.BALANCE
                    });
                }
                catch (Exception ex)
                {

                }
                return RedirectToAction("PaymentFail", "PaymentResult");
            }

            if (mdstatus.Equals("1") || mdstatus.Equals("2") || mdstatus.Equals("3") || mdstatus.Equals("4"))
            {
                try
                {
                    string transId = Request.Form["TransId"];
                    string errMsg = Request.Form["ErrMsg"];

                    if (Request.Form["Response"] == "Approved")
                    {
                        var dto = new PaymentDto
                        {
                            documentId = 0,
                            documentNumber = "Bakiye",
                            paymentDate = Convert.ToDateTime(DateTime.Now.ToShortDateString()),
                            paymentType = PaymentTypeHelper.BALANCE,
                            transId = transId,
                            userId = Convert.ToInt32(Request.Form["firmaadi"]),
                            path = $"{HostingEnvironment.WebRootPath}\\logorder\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{Request.Form["firmaadi"]}.xml",
                            paymentTotal = Convert.ToDouble(Request.Form["amount"].ToString().Replace(".", ",")),
                            oid = Request.Form["oid"],
                            message = MessageHelper.PAYMENT_SAVE_SUCCESS,
                        };
                        var model = PaymentService.PaymentSave(dto);
                        TempData["messageModel"] = JsonConvert.SerializeObject(dto);
                        return RedirectToAction("PaymentComplete", "PaymentResult");
                    }
                    else
                    {
                        try
                        {
                            TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                            {
                                message = errMsg,
                                oid = Request.Form["oid"],
                                userId = Convert.ToInt32(Request.Form["firmaadi"]),
                                transId = Request.Form["TransId"],
                                amount = Request.Form["amount"],
                                paymentType = PaymentTypeHelper.BALANCE
                            });
                        }
                        catch (Exception ex)
                        {

                        }
                        return RedirectToAction("PaymentFail", "PaymentResult");
                    }
                }
                catch (Exception ex)
                {
                    try
                    {
                        JsonConvert.SerializeObject(TempData["messageModel"] = new PaymentDto
                        {
                            message = ex.Message,
                            oid = Request.Form["oid"],
                            userId = Convert.ToInt32(Request.Form["firmaadi"]),
                            transId = Request.Form["TransId"],
                            amount = Request.Form["amount"],
                            paymentType = PaymentTypeHelper.BALANCE
                        });
                    }
                    catch (Exception ex1)
                    {

                    }
                    return RedirectToAction("PaymentFail", "PaymentResult");
                }
            }
            else
            {
                try
                {
                    TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                    {
                        message = MessageHelper.PAYMENT_3D_ERROR,
                        oid = Request.Form["oid"],
                        userId = Convert.ToInt32(Request.Form["firmaadi"]),
                        transId = Request.Form["TransId"],
                        amount = Request.Form["amount"],
                        paymentType = PaymentTypeHelper.BALANCE
                    });
                }
                catch (Exception ex1)
                {

                }
                return RedirectToAction("PaymentFail", "PaymentResult");
            }

        }*/

        /*public IActionResult FailUrl()
        {
            try
            {
                string errMsg = Request.Form["ErrMsg"];
                TempData["messageModel"] = JsonConvert.SerializeObject(new PaymentDto
                {
                    message = string.IsNullOrEmpty(errMsg) ? MessageHelper.SORRY_PAYMENTSTART_MESSAGE_ERROR : errMsg,
                    oid = Request.Form["oid"],
                    userId = Convert.ToInt32(Request.Form["firmaadi"]),
                    transId = Request.Form["TransId"],
                    amount = Request.Form["amount"],
                    paymentType = PaymentTypeHelper.BALANCE
                });
            }
            catch (Exception ex1)
            {

            }
            return RedirectToAction("PaymentFail", "PaymentResult");
        }*/

        /*public IActionResult PaymentFail()
        {
            PaymentDto paymentDto = null;
            if(TempData["messageModel"] != null)
            {
                paymentDto = JsonConvert.DeserializeObject<PaymentDto>(TempData["messageModel"].ToString());
                Log.log($"{HostingEnvironment.WebRootPath}\\logbank\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{paymentDto.userId}.xml", paymentDto);
            }
                
            return View(paymentDto);
        }*/
        /*public IActionResult PaymentComplete()
        {
            PaymentDto paymentDto = null;
            if (TempData["messageModel"] != null)
            {
                paymentDto = JsonConvert.DeserializeObject<PaymentDto>(TempData["messageModel"].ToString());
            }
            return View(paymentDto);
        }*/
    }
}