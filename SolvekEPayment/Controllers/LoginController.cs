﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.SendInfos;
using SolvekEPayment.Services.AppServices.ForgotServices;
using SolvekEPayment.Services.AppServices.LoginServices;

namespace SolvekEPayment.Controllers
{
    [Authorize]
    public class LoginController : Controller
    {
        private readonly ILoginService LoginService;
        private readonly IForgotService ForgotService;
        private readonly ISendInfo SendInfo;
        private readonly IHostingEnvironment HostingEnvironment;
        public LoginController(ILoginService loginService, IForgotService forgotService, 
            ISendInfo sendInfo, IHostingEnvironment hostingEnvironment)
        {
            LoginService = loginService;
            ForgotService = forgotService;
            SendInfo = sendInfo;
            HostingEnvironment = hostingEnvironment;
        }

        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Login(LoginDto loginDto)
        {
            if (!ModelState.IsValid)
                return View();

           
            var result = LoginService.Login(loginDto);
            if (result.result)
            {
                try
                {
                    var identity = new ClaimsIdentity(new[] {
                    new Claim(ClaimTypes.Name, loginDto.userName)
                        }, CookieAuthenticationDefaults.AuthenticationScheme);
                    var principal = new ClaimsPrincipal(identity);
                    var props = new AuthenticationProperties
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTimeOffset.Now.AddMinutes(30)
                    };
                    var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, props);
                    CookieOptions cookieOptions = new CookieOptions();
                    cookieOptions.Expires = DateTimeOffset.MaxValue;
                    cookieOptions.IsEssential = true;
                    Response.Cookies.Append("userId", result.userId.ToString(), cookieOptions);
                    Response.Cookies.Append("userName", loginDto.userName.ToString(), cookieOptions);
                    return RedirectToAction("Index", "Home");
                }
                catch (Exception ex)
                {
                    ViewData["error"] = MessageHelper.LOGIN_ERROR;
                }
            }
            ViewData["error"] = result.message;
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Logout()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            Response.Cookies.Delete("userId");
            Response.Cookies.Delete("userName");
            return RedirectToAction("Login", "Login");
        }

        [AllowAnonymous]
        public IActionResult Forgot()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("ForgotSend")]
        public IActionResult ForgotSend(ForgotDto forgotDto)
        {
            if (!ModelState.IsValid)
                return View();

            var model = ForgotService.Forgot(forgotDto);
            if (model.result)
            {
                SendInfo.send(new SendForgotEmailDto
                {
                    customerName = model.customerName,
                    emails = new List<string> { model.email },
                    password = model.password,
                    userName =model.userName,
                    path = $@"{HostingEnvironment.WebRootPath}\\content\\appdata\\UserEmailLayout.html"
                });
            }
            return Json(ForgotService.Forgot(forgotDto));
        }
    }
}