﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Services.AppServices.LoginServices;

namespace SolvekEPayment.Controllers
{
    [Authorize]
    public class ChangePasswordController : Controller
    {
        private readonly ILoginService LoginService;
        public ChangePasswordController(ILoginService loginService)
        {
            LoginService = loginService;
        }

        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ChangePassword(ChangePasswordDto changePasswordDto)
        {
            if (!ModelState.IsValid)
                return View(new { result = false, message = MessageHelper.SORRY_MESSAGE_ERROR });

            changePasswordDto.userId = Convert.ToInt32(Request.Cookies["userId"]);
            var result = LoginService.ChangePassword(changePasswordDto);
            if (result.result)
            {
                ViewData["userName"] = ViewData["password"] = ViewData["passwordAgain"] = "";
            }
            else
            {
                ViewData["userName"] = changePasswordDto.userName;
                ViewData["password"] = changePasswordDto.password;
                ViewData["passwordAgain"] = changePasswordDto.passwordAgain;
            }
            ModelState.Clear();
            return View(LoginService.ChangePassword(changePasswordDto));
        }
    }
}