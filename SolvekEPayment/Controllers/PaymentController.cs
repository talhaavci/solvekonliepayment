﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using SolvekEPayment.Banks;
using SolvekEPayment.Banks.Akbank;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Dtos.ErpDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Services.ErpServices.CustomerServices;

namespace SolvekEPayment.Controllers
{
    [Authorize]
    public class PaymentController : Controller
    {
        private readonly ICustomerService CustomerService;
        private readonly IPaymentStart PaymentStart;
        private readonly IHostingEnvironment HostingEnvironment;
        private readonly IPaymentInitControl PaymentInitControl;
        public PaymentController(ICustomerService customerService, IPaymentStart paymentStart,
            IHostingEnvironment hostingEnvironment, IPaymentInitControl paymentInitControl)
        {
            CustomerService = customerService;
            PaymentStart = paymentStart;
            HostingEnvironment = hostingEnvironment;
            PaymentInitControl = paymentInitControl;
        }

        /*public IActionResult Payment()
        {
            TempData["result"] = false;
            return View();
        }*/

        /*public IActionResult PaymentInvoice(int id)
        {
            return View(CustomerService.InvoiceTitle(new InvoiceTitleDto
            {
                id = id,
                userId = Convert.ToInt32(Request.Cookies["userId"])
            }));
        }*/

        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult PaymentInvoice(PaymentStartDto paymentStartDto)
        {
            if (!ModelState.IsValid)
                return View();

            paymentStartDto.path = HostingEnvironment.WebRootPath;
            paymentStartDto.userId = Convert.ToInt32(Request.Cookies["userId"]);
            paymentStartDto.pan = paymentStartDto.pan.Trim().Replace(" ", "");
            paymentStartDto.paymentType = PaymentTypeHelper.INVOICE;

            var controlModel = PaymentInitControl.control(paymentStartDto);
            if (controlModel.result)
            {
                paymentStartDto.pan = paymentStartDto.pan.Trim().Replace(" ", "");
                var result = PaymentStart.paymentStart(paymentStartDto);
                if (result.result)
                {
                    TempData["result"] = false;
                    byte[] bytes = Encoding.ASCII.GetBytes(result.message);
                    HttpContext.Response.Body.Write(bytes);
                }
                else
                {
                    TempData["result"] = true;
                    TempData["message"] = result.message;
                }
            }
            else
            {
                TempData["result"] = true;
                TempData["message"] = controlModel.message;
            }
            return RedirectToAction("PaymentInvoice", new { id = paymentStartDto.id });
        }*/

        /*[HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Payment(PaymentStartDto paymentStartDto)
        {
            if (!ModelState.IsValid)
                return View();

            paymentStartDto.userId = Convert.ToInt32(Request.Cookies["userId"]);
            paymentStartDto.paymentType = PaymentTypeHelper.BALANCE;
            paymentStartDto.path = HostingEnvironment.WebRootPath;
            var controlModel = PaymentInitControl.control(paymentStartDto);
            if (controlModel.result)
            {
                paymentStartDto.pan = paymentStartDto.pan.Trim().Replace(" ", "");
                paymentStartDto.documentNumber = "";
                var result = PaymentStart.paymentStart(paymentStartDto);
                if (result.result)
                {
                    TempData["result"] = false;
                    byte[] bytes = Encoding.ASCII.GetBytes(result.message);
                    HttpContext.Response.Body.Write(bytes);
                }
                else
                {
                    TempData["result"] = true;
                    TempData["message"] = result.message;
                }
            }
            else
            {
                TempData["result"] = true;
                TempData["message"] = controlModel.message;
            }
            return View();
        }*/
    }
}