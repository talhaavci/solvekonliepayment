﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SolvekEPayment.Dtos;
using SolvekEPayment.Models;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models.ErpModels;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Dtos.ErpDtos;

namespace SolvekEPayment.Repositories.ErpRepositories.CustomerRespositories
{
    public class CustomerRepository : ICustomerRepository
    {
        IConfiguration Configuration;
        public CustomerRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public PageViewModel<CustomerReceiptBalanceModelResponse> CustomerReceiptBalances(CustomerBalanceDto customerBalanceDto)
        {
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database").GetSection("ErpDatabase").Value))
            {
                try
                {
                    string paymentDbName = Configuration.GetSection("PaymentDbName").Value;
                    double sumTotal = 0;
                    var total = dbConnection.ExecuteScalar($@"select CARALACAKTOP from
                    CARKART WITH (NOLOCK) where 1=1and 1=1  and CARKOD >= isnull((select top 1 customerCode from {paymentDbName}..Users where id=@userId), '')
                    and CARKOD <= isnull((select top 1 customerCode from {paymentDbName}..Users where id=@userId), '') and CARCALFLAG in (0,1,2)",
                    new { customerBalanceDto.userId });

                    if (total != null)
                        sumTotal = Convert.ToDouble(total);

                    var result = dbConnection.Query<CustomerReceiptBalanceModelResponse>($@"
                        select CONVERT(VARCHAR, CARHARTAR, 104) as date, CARHARTAR as tempDate, CARHARTUTAR AS total, 
                        CARHAREVRAKNO AS documentNumber, CARHARREFNO AS id, CARHARTUTAR as remainTotal
                        from
                        CARHAR WITH (NOLOCK) 
                        where 1=1
                        AND CARHARCARKOD=isnull((select top 1 customerCode from {paymentDbName}..Users where id=@userId), '')
                        AND CARHARGCFLAG=1
                        and CARHAR.CARHARTIPI in (1,2,3,4,5,6,7,8) 
                        and CARHAR.CARHARISTIPNO in (1,2,3,4,5,6,7) 
                        and CARHAR.CARHARKAYNAK in (2,3,5,38,39) 
                        order by CARHARTAR",
                        new
                        {
                            searchStr = $"%{(customerBalanceDto.search.value == null ? "" : customerBalanceDto.search.value.ToUpper())}%",
                            customerBalanceDto.length,
                            customerBalanceDto.start,
                            customerBalanceDto.userId
                        }).ToList();

                    foreach (var item in result)
                    {
                        if (sumTotal > 0)
                        {
                            if (item.remainTotal <= sumTotal)
                            {
                                sumTotal = sumTotal - item.remainTotal;
                                item.remainTotal = 0;
                            }
                            else
                            {
                                item.remainTotal = item.remainTotal - sumTotal;
                                sumTotal = 0;
                            }
                        }
                    }

                    result = result.AsQueryable().Where(x => x.remainTotal > 0).OrderBy(o => o.tempDate).ToList();
                    var model = new PageViewModel<CustomerReceiptBalanceModelResponse>();
                    model.result = true;
                    model.message = MessageHelper.OK;
                    model.data = result;
                    model.draw = customerBalanceDto.draw;
                    model.recordsTotal = model.recordsFiltered = result.Count;
                    return model;
                }
                catch (Exception ex)
                {
                    return new PageViewModel<CustomerReceiptBalanceModelResponse>
                    {
                        result = false,
                        message = "",
                        draw = customerBalanceDto.draw,
                        data = new List<CustomerReceiptBalanceModelResponse>()
                    };
                }
            }
        }

        public CustomerBalanceTotalModelResponse CustomerBalanceTotal(int userId)
        {
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database").GetSection("ErpDatabase").Value))
            {
                try
                {
                    string paymentDbName = Configuration.GetSection("PaymentDbName").Value;
                    return dbConnection.Query<CustomerBalanceTotalModelResponse>($@"SELECT 
                        isnull(SUM(isnull((CASE WHEN CARHARGCFLAG = 1 THEN CARHARTUTAR ELSE 0 END), 0) - 
                        isnull((CASE WHEN CARHARGCFLAG = 2 THEN CARHARTUTAR ELSE 0 END), 0)), 0) AS total, 'TL' as curName  
                        FROM CARHAR WHERE CARHARCARKOD = isnull((select top 1 customerCode from {paymentDbName}..Users 
                        where id=@userId), '')", new { userId }).FirstOrDefault();

                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public InvoiceTitleModelResponse InvoiceTitle(InvoiceTitleDto ınvoiceTitleDto)
        {
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database").GetSection("ErpDatabase").Value))
            {
                try
                {
                    string paymentDbName = Configuration.GetSection("PaymentDbName").Value;
                    return dbConnection.Query<InvoiceTitleModelResponse>($@"select FATFISREFNO as id, CONVERT(VARCHAR, FATFISTAR, 104) AS date, 
                        FATFISEVRAKNO1 AS documentNumber, 
                        FATFISGENTOPLAM-isnull((select sum(paymentTotal) from {paymentDbName}..Payments where documentNumber=FATFISEVRAKNO1),0) as total, 
                        'TL' as curName from FATFIS where FATFISREFNO=@id 
                        and FATFISCARKOD=isnull((select top 1 customerCode from {paymentDbName}..Users where id=@userId), '')",
                        ınvoiceTitleDto).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public PageViewModel<CustomerBalanceModelResponse> CustomerBalances(CustomerBalanceDto customerBalanceDto)
        {
            string paymentDbName = Configuration.GetSection("PaymentDbName").Value;
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database").GetSection("ErpDatabase").Value))
            {
                try
                {
                    double sumTotal = 0, total = 0;
                    object result = dbConnection.ExecuteScalar($@"SELECT  
                        isnull(SUM(isnull((CASE WHEN CARHARGCFLAG = 1 THEN CARHARTUTAR ELSE 0 END), 0) - isnull((CASE WHEN CARHARGCFLAG = 2 THEN CARHARTUTAR ELSE 0 END), 0)), 0) AS balance  
                        FROM  CARHAR 
                        WHERE (CARHARTAR < @startDate) and CARHARCARKOD = isnull((select top 1 customerCode from {paymentDbName}..Users where id=@userId), '')",
                        new
                        {
                            customerBalanceDto.userId,
                            customerBalanceDto.startDate
                        });

                    if (result != null)
                        sumTotal = Convert.ToDouble(result);
                    total = sumTotal;

                    var balanceList = dbConnection.Query<CustomerBalanceModelResponse>($@"SELECT CONVERT(VARCHAR, B.CARHARTAR, 104) AS date
                                    ,B.CARHARACIKLAMA AS type
                                    ,(CASE WHEN B.CARHARGCFLAG = 1 THEN B.CARHARTUTAR ELSE 0 END) AS debit
                                    ,(CASE WHEN B.CARHARGCFLAG = 2 THEN B.CARHARTUTAR ELSE 0 END) AS credit
                                    ,0 as total
                                    ,B.CARHAREVRAKNO AS documentNumber  
                                    FROM CARKART AS A 
                                    LEFT OUTER JOIN CARHAR AS B
                                    ON A.CARKOD=B.CARHARCARKOD 
                                    WHERE 1=1 
                                    and (B.CARHARTAR BETWEEN @startDate AND @endDate) and upper(B.CARHAREVRAKNO) like @searchStr
                                    AND A.CARKOD=isnull((select top 1 customerCode from {paymentDbName}..Users where id=@userId), '') 
                                    order by B.CARHARTAR asc",
                                    new
                                    {
                                        customerBalanceDto.userId,
                                        customerBalanceDto.startDate,
                                        customerBalanceDto.endDate,
                                        searchStr = $"%{(customerBalanceDto.searchStr == null ? "" : customerBalanceDto.searchStr)}%"
                                    }).ToList();

                    balanceList.ForEach(x => { x.total = sumTotal += (x.debit - x.credit); });

                    if (total != 0)
                        balanceList.Insert(0, new CustomerBalanceModelResponse
                        {
                            date = customerBalanceDto.startDate.Value.ToString("dd/MM/yyyy"),
                            documentNumber = "",
                            type = "Yekün Toplam",
                            total = total,
                            debit = total > 0 ? total : 0,
                            credit = total < 0 ? (-1 * total) : 0
                        });

                    return new PageViewModel<CustomerBalanceModelResponse>
                    {
                        data = balanceList,
                        draw = customerBalanceDto.draw,
                        recordsFiltered = balanceList.Count,
                        recordsTotal = balanceList.Count,
                        result = true,
                        message = MessageHelper.OK
                    };
                }
                catch (Exception ex)
                {
                    return new PageViewModel<CustomerBalanceModelResponse>
                    {
                        data = new List<CustomerBalanceModelResponse>(),
                        draw = customerBalanceDto.draw,
                        recordsFiltered = 0,
                        recordsTotal = 0,
                        result = true,
                        message = MessageHelper.OK
                    };
                }
            }
        }
    }
}
