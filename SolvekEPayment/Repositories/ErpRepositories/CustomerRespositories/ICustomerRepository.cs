﻿using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Dtos.ErpDtos;
using SolvekEPayment.Models;
using SolvekEPayment.Models.ErpModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Repositories.ErpRepositories.CustomerRespositories
{
    public interface ICustomerRepository
    {
        PageViewModel<CustomerReceiptBalanceModelResponse> CustomerReceiptBalances(CustomerBalanceDto customerBalanceDto);
        PageViewModel<CustomerBalanceModelResponse> CustomerBalances(CustomerBalanceDto customerBalanceDto);
        CustomerBalanceTotalModelResponse CustomerBalanceTotal(int userId);
        InvoiceTitleModelResponse InvoiceTitle(InvoiceTitleDto ınvoiceTitleDto);
    }
}
