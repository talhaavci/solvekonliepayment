﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models.AppModels;

namespace SolvekEPayment.Repositories.AppRepositories.ForgotRepositories
{
    public class ForgotRepository : IForgotRepository
    {
        IConfiguration Configuration;
        public ForgotRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public ForgotModelResponse Forgot(ForgotDto forgotDto)
        {
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database").GetSection("SolvekDatabase").Value))
            {
                try
                {
                    var result = dbConnection.Query<ForgotModelResponse>(@"select cast(1 as bit) as result, email as email,
                        password, userName, customerName from Users 
                        where userName=@userName", forgotDto).FirstOrDefault();

                    if (result == null)
                        return new ForgotModelResponse { result = false, message = MessageHelper.FORGOT_USERNAME_NOT_FOUND };

                    result.result = true;
                    result.message = $"Bilgileriniz {result.email} adresine gönderilmiştir.";
                    return result;
                }
                catch (Exception ex)
                {
                    return new ForgotModelResponse { result = false, message = MessageHelper.FORGOT_ERROR };
                }
            }
        }
    }
}
