﻿using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models;
using SolvekEPayment.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Repositories.AppRepositories.ForgotRepositories
{
    public interface IForgotRepository
    {
        ForgotModelResponse Forgot(ForgotDto forgotDto);
    }
}
