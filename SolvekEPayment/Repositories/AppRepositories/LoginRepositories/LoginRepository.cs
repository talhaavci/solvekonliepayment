﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models.AppModels;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models;
using System.Net;

namespace SolvekEPayment.Repositories.AppRepositories.LoginRepositories
{
    public class LoginRepository : ILoginRepository
    {
        IConfiguration Configuration;
        public LoginRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public LoginModelResponse Login(LoginDto loginDto)
        {
            //ServicePointManager.Expect100Continue = true;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database").GetSection("SolvekDatabase").Value))
            {
                try
                {
                    var result = dbConnection.Query<LoginModelResponse>($@"declare @count int
                    declare @userId bigint
                    select @count=count(*), @userId=id from Users where userName=@userName and password=@password group by id
                    if @count > 0 begin
                        select cast(1 as bit) as result, '{MessageHelper.OK}' as message, @userId as userId
                    end else begin
                        select cast(0 as bit) as result, '{MessageHelper.LOGIN_WRONG_ERROR}' as message, 0 as userId
                    end", loginDto).FirstOrDefault();

                    if(result == null)
                        return new LoginModelResponse { result = false, message = MessageHelper.LOGIN_ERROR, userId = 0 };

                    return result;
                }
                catch (Exception ex)
                {
                    return new LoginModelResponse { result = false, message = MessageHelper.LOGIN_ERROR, userId = 0 };
                }
            }
        }

        public MessageModelResponse ChangePassword(ChangePasswordDto changePasswordDto)
        {
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database").GetSection("SolvekDatabase").Value))
            {
                try
                {
                    return  dbConnection.Query<MessageModelResponse>($@"declare @count int
                    select @count=count(*) from Users where userName=@userName and id=@userId
                    if @count > 0 begin
                        update Users set password=@password where userName=@userName and id=@userId
                        select cast(1 as bit) as result, '{MessageHelper.CHANGE_PASSWORD_SUCCESS}' as message
                    end else begin
                        select cast(0 as bit) as result, '{MessageHelper.CHANGE_USERNAME_MISMATCH}' as message
                    end", changePasswordDto).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return new MessageModelResponse { result = false, message = MessageHelper.SORRY_MESSAGE_ERROR };
                }
            }
        }
    }
}
