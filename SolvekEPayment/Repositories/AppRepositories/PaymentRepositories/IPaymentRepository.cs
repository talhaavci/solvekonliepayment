﻿using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Repositories.AppRepositories.PaymentRepositories
{
    public interface IPaymentRepository
    {
        MessageModelResponse PaymentSave(PaymentDto paymentDto);
    }
}
