﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using SolvekEPayment.Helpers;
using SolvekEPayment.SolvekLog;

namespace SolvekEPayment.Repositories.AppRepositories.PaymentRepositories
{
    public class PaymentRepository : IPaymentRepository
    {
        IConfiguration Configuration;
        private readonly ILog Log;
        public PaymentRepository(IConfiguration configuration, ILog log)
        {
            Configuration = configuration;
            Log = log;
        }
        public MessageModelResponse PaymentSave(PaymentDto paymentDto)
        {
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database").GetSection("SolvekDatabase").Value))
            {
                try
                {
                    dbConnection.Execute(@"insert into Payments(userId, paymentDate, documentId,
                    documentNumber, oid, paymentTotal, paymentType, transId, installment, bankId, cardId) 
                    values(@userId, @paymentDate, @documentId, @documentNumber, 
                    @oid, @paymentTotal, @paymentType, @transId, @installment, @bankId, @cardId)", paymentDto);
                    return new MessageModelResponse { result = true, message = MessageHelper.PAYMENT_SAVE_SUCCESS };
                }
                catch (Exception ex)
                {
                    paymentDto.message = ex.Message;
                    Log.log(paymentDto.path, paymentDto);
                    return new MessageModelResponse { result = false, message = MessageHelper.SORRY_PAYMENTSAVE_ERROR };
                }
            }
        }
    }
}
