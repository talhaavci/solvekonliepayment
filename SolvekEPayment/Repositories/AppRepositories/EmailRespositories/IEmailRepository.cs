﻿using SolvekEPayment.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Repositories.AppRepositories.EmailRespositories
{
    public interface IEmailRepository
    {
        EmailModelResponse Email();
    }
}
