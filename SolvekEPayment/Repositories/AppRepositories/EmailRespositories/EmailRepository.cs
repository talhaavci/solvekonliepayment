﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SolvekEPayment.Models.AppModels;
using Dapper;
using System.Data;
using System.Data.SqlClient;

namespace SolvekEPayment.Repositories.AppRepositories.EmailRespositories
{
    public class EmailRepository : IEmailRepository
    {
        IConfiguration Configuration;
        public EmailRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public EmailModelResponse Email()
        {
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database").GetSection("SolvekDatabase").Value))
            {
                try
                {
                    return dbConnection.Query<EmailModelResponse>(@"select * from EmailSetting").FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
    }
}
