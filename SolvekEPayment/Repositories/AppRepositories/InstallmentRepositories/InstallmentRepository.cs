﻿using Microsoft.Extensions.Configuration;
using SolvekEPayment.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using System.Data.SqlClient;
using System.Data;

namespace SolvekEPayment.Repositories.AppRepositories.InstallmentRepositories
{
    public class InstallmentRepository : IInstallmentRepository
    {
        IConfiguration Configuration;
        public InstallmentRepository(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public async Task<InstallmentModelResponse> Installment(int userId)
        {
            using (IDbConnection dbConnection = new SqlConnection(Configuration.GetSection("Database")
                .GetSection("SolvekDatabase").Value))
            {
                try
                {
                    var result = await dbConnection.QueryAsync<InstallmentModelResponse>($@"select installment from Users where id=@userId", 
                        new { userId });

                    return result.FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
    }
}
