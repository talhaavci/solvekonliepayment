﻿using SolvekEPayment.Models.AppModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Repositories.AppRepositories.InstallmentRepositories
{
    public interface IInstallmentRepository
    {
        Task<InstallmentModelResponse> Installment(int userId);
    }
}
