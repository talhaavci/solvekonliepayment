﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SolvekEPayment.Xml
{
    public class XmlWrite<T> where T : class
    {
        public void Write(string path, T entity)
        {
            try
            {
                if (!File.Exists(path))
                    File.Create(path).Dispose();
                using (TextWriter textWriter = new StreamWriter(path, false))
                {
                    XmlSerializer xmlSerializer = new XmlSerializer(typeof(T));
                    xmlSerializer.Serialize(textWriter, entity);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
