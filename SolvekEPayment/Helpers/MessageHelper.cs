﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Helpers
{
    public class MessageHelper
    {
        public static string OK = "OK";
        public static string LOGIN_ERROR = "Giriş yapılamadı. Lütfen tekrar deneyin.";
        public static string LOGIN_WRONG_ERROR = "Kullanıcı adı veya şifre yanlış.";
        public static string FORGOT_ERROR = "Üzgünüz! Bir şeyler ters gitti. Lütfen tekrar deneyin.";
        public static string SORRY_MESSAGE_ERROR = "Üzgünüz! Bir şeyler ters gitti. Lütfen tekrar deneyin.";
        public static string SORRY_PAYMENTSAVE_ERROR = "Ödeme başarı ile alındı. Tahsilat işlemi kayıt edilemedi. Lütfen firma ile iletişime geçiniz.";
        public static string SORRY_PAYMENTSTART_MESSAGE_ERROR = "İşlem Başarısız! Lütfen tekrar deneyin.";
        public static string FORGOT_USERNAME_NOT_FOUND = "Girilen kullanıcı adı sistemde bulunamadı. Lütfen kontrol ediniz.";
        public static string PAYMENT_SAVE_SUCCESS = "Ödeme başarı ile alındı. Yaklaşık 3 dakika içinde sistemimize işlenecektir.";
        public static string PAYMENT_HASH_ERROR = "Hash değeri uyuşmadı. Hesabınızı kontrol ediniz ve herhangi bir problem halinde firma ile iletişime geçiniz.";
        public static string PAYMENT_HASH_EMPTY_ERROR = "Hash değeri boş. Hesabınızı kontrol ediniz ve herhangi bir problem halinde firma ile iletişime geçiniz.";
        public static string PAYMENT_3D_ERROR = "3D doğrulaması başarısız. Lütfen tekrar deneyiniz.";
        public static string CHANGE_PASSWORD_SUCCESS = "Şifreniz başarıyla değiştirildi.";
        public static string CHANGE_USERNAME_MISMATCH = "İşlem başarısız. Girilen kullanıcı adının sizin olduğundan emin olun.";
        public static string CHANGE_PASSWORD_MISMATCH = "Şifreler uyuşmuyor. Lütfen kontrol edin.";
        public static string CHANGE_PASSWORD_LENGTH = "Şifreniz en az 6 haneden oluşmalıdır.";
        public static string NOT_EMPTY = "İlgili alanları doldurunuz.";
        public static string TOTAL_ERROR = "Ödenen tutar 1 TL veya 1 TL'den büyük olmak zorundadır.";
        public static string USER_ERROR = "Kullanıcı tanımlanamadı.";
        public static string PAN_ERROR = "Kart numarasını eksiksiz giriniz.";
        public static string CVV_ERROR = "Güvenlik kodunu eksiksiz giriniz.";
        public static string EXPIRE_ERROR = "Son kullanma tarihini eksiksiz giriniz.";
        public static string INSTALLMENT_ERROR = "Tasit sayısı seçiniz.";
        public static string BANK_ERROR = "Banka seçiniz.";
        public static string CARD_ERROR = "Kart tipi seçiniz.";
    }
}
