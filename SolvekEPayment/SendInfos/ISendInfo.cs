﻿using SolvekEPayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.SendInfos
{
    public interface ISendInfo
    {
        MessageModelResponse send<T>(T value) where T : class;
    }
}
