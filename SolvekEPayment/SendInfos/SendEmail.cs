﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models;

namespace SolvekEPayment.SendInfos
{
    public class SendEmail : ISendInfo
    {
        private readonly ISendEmailHelper SendEmailHelper;
        private readonly IConfiguration Configuration;
        public SendEmail(ISendEmailHelper sendEmailHelper, IConfiguration configuration)
        {
            SendEmailHelper = sendEmailHelper;
            Configuration = configuration;
        }
        public MessageModelResponse send<T>(T value) where T : class
        {
            string body = "";
            SendForgotEmailDto sendForgotEmailDto = value as SendForgotEmailDto;
            using (StreamReader reader = new StreamReader(sendForgotEmailDto.path))
            {
                body = reader.ReadToEnd();
                body = body.Replace("[CompanyName]", sendForgotEmailDto.customerName);
                body = body.Replace("[UserName]", sendForgotEmailDto.userName);
                body = body.Replace("[Password]", sendForgotEmailDto.password);
            }
            return new MessageModelResponse
            {
                result = SendEmailHelper.sendEmail(new SendEmailBaseDto
                {
                    displayName = Configuration.GetSection("emailDisplayName").Value,
                    emails = sendForgotEmailDto.emails,
                    subject = $"{Configuration.GetSection("appName").Value} Şifre Bilgisi",
                    body = body
                }),
                message = MessageHelper.OK
            };
        }
    }
}
