﻿using SolvekEPayment.Dtos.AppDtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.SendInfos
{
    public interface ISendEmailHelper
    {
        bool sendEmail(SendEmailBaseDto sendEmailBaseDto);
    }
}
