﻿using Microsoft.Extensions.Configuration;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Services.AppServices.EmailServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace SolvekEPayment.SendInfos
{
    public class SendEmailHelper : ISendEmailHelper
    {
        private readonly IConfiguration Configuration;
        private readonly IEmailService EmailService;
        public SendEmailHelper(IConfiguration configuration, IEmailService emailService)
        {
            Configuration = configuration;
            EmailService = emailService;
        }
        public bool sendEmail(SendEmailBaseDto sendEmailBaseDto)
        {
            try
            {
                var mailSetting = EmailService.Email();
                if (mailSetting == null)
                    return false;

                SmtpClient SmtpMail = new SmtpClient(mailSetting.smtp, mailSetting.port);
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(mailSetting.email, mailSetting.password);
                SmtpMail.UseDefaultCredentials = false;
                SmtpMail.Credentials = basicAuthenticationInfo;
                SmtpMail.EnableSsl = Convert.ToBoolean(Configuration.GetSection("ssl").Value.ToString());

                MailMessage eMail = new MailMessage();
                eMail.From = new MailAddress(mailSetting.email, sendEmailBaseDto.displayName);
                foreach (var item in sendEmailBaseDto.emails)
                {
                    eMail.To.Add(item);
                }
                eMail.BodyEncoding = System.Text.Encoding.GetEncoding("utf-8");
                eMail.Subject = sendEmailBaseDto.subject;
                eMail.IsBodyHtml = true;
                eMail.Body = sendEmailBaseDto.body;
                SmtpMail.Send(eMail);
                eMail.Dispose();
                SmtpMail.Dispose();

                return true;
            }
            catch (Exception x)
            {
                return false;
            }
        }
    }
}
