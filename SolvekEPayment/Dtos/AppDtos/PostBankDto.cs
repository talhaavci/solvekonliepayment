﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Dtos.AppDtos
{
    public class PostBankDto
    {
        public string postData { get; set; }
        public string postUrl { get; set; }
        public string path { get; set; }
    }
}
