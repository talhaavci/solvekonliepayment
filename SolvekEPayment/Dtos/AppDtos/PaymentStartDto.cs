﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Dtos.AppDtos
{
    public class PaymentStartDto
    {
        public int userId { get; set; }
        public string holder { get; set; }
        public string pan { get; set; }
        public string cv { get; set; }
        public string expire { get; set; }
        public string description { get; set; }
        public double paymentTotal { get; set; }
        public string documentNumber { get; set; }
        public string path { get; set; }
        public int installment { get; set; }
        public int paymentType { get; set; }
        public int id { get; set; }
        public int bankId { get; set; }
        public int cardId { get; set; }
    }
}
