﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Dtos.AppDtos
{
    public class ChangePasswordDto
    {
        public int userId { get; set; }
        public string userName { get; set; }
        public string password { get; set; }
        public string passwordAgain { get; set; }
    }
}
