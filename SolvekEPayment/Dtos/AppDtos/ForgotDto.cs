﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Dtos.AppDtos
{
    public class ForgotDto
    {
        [Display(Name = "Cari kodunuzu giriniz")]
        [Required(ErrorMessage = "Lütfen ilgili alanı doldurun.")]
        public string userName { get; set; }
    }
}
