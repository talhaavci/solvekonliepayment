﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Dtos.AppDtos
{
    public class LoginDto
    {
        [Display(Name = "Cari kodunuzu giriniz")]
        [Required(ErrorMessage = "Kullanıcı adı boş geçilemez.")]
        public string userName { get; set; }
        [Display(Name = "Şifrenizi giriniz")]
        [Required(ErrorMessage = "Şifre boş geçilemez.")]
        public string password { get; set; }
    }
}
