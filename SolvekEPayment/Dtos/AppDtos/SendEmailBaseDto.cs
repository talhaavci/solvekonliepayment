﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Dtos.AppDtos
{
    public class SendEmailBaseDto
    {
        public List<string> emails { get; set; }
        public string body { get; set; }
        public string subject { get; set; }
        public string displayName { get; set; }
    }
}
