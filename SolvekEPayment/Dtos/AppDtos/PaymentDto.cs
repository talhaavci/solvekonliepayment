﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Dtos.AppDtos
{
    public class PaymentDto
    {
        public string oid { get; set; }
        public int userId { get; set; }
        public DateTime paymentDate { get; set; }
        public int documentId { get; set; }
        public string documentNumber { get; set; }
        public string amount { get; set; }
        public double paymentTotal { get; set; }
        public int paymentType { get; set; }
        public string installment { get; set; }
        public string transId { get; set; }
        public string path { get; set; }
        public string message { get; set; }
        public int cardId { get; set; }
        public int bankId { get; set; }
    }
}
