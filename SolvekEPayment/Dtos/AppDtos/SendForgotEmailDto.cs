﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Dtos.AppDtos
{
    public class SendForgotEmailDto
    {
        public string userName { get; set; }
        public string password { get; set; }
        public string customerName { get; set; }
        public string path { get; set; }
        public List<string> emails { get; set; }
    }
}
