﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Dtos.ErpDtos
{
    public class CustomerBalanceDto : BaseDto
    {
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public string searchStr { get; set; }
        public int userId { get; set; }
    }
}
