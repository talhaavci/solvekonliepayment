﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Extensions.Configuration;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models;
using SolvekEPayment.SolvekLog;

namespace SolvekEPayment.Banks.Vakifbank
{
    public class VakifbankPaymentStart : IPaymentStart
    {
        private readonly ILog Log;
        private readonly IPaymentRequest PaymentRequest;
        private readonly IConfiguration Configuration;
        string _3dSecureServerUrl = "https://3dsecure.vakifbank.com.tr:4443/MPIAPI/MPI_Enrollment.aspx";
        public VakifbankPaymentStart(ILog log, IPaymentRequest paymentRequest, IConfiguration configuration)
        {
            PaymentRequest = paymentRequest;
            Log = log;
            Configuration = configuration;
        }
        public MessageModelResponse paymentStart<T>(T value) where T : class
        {
            PaymentStartDto paymentStartDto = value as PaymentStartDto;
            try
            {
                string oid = Guid.NewGuid().ToString();
                string okUrl = $"{(paymentStartDto.paymentType == PaymentTypeHelper.BALANCE ? Configuration.GetSection("paymentOkUrl").Value : Configuration.GetSection("paymentInvoiceOkUrl").Value)}?bType=vakifbank_{paymentStartDto.userId}_{paymentStartDto.bankId}_{paymentStartDto.cardId}";
                string failUrl = $"{(paymentStartDto.paymentType == PaymentTypeHelper.BALANCE ? Configuration.GetSection("paymentFailUrl").Value : Configuration.GetSection("paymentInvoiceFailUrl").Value)}?bType=vakifbank_{paymentStartDto.userId}_{paymentStartDto.bankId}_{paymentStartDto.cardId}";
                string installment = paymentStartDto.installment == 1 || paymentStartDto.installment == 0 ? "" : paymentStartDto.installment.ToString();
                string[] expires = paymentStartDto.expire.Split('/');

                string post = $"Pan={paymentStartDto.pan}&ExpiryDate={Convert.ToInt32(expires[1]).ToString("00")}{Convert.ToInt32(expires[0]).ToString("00")}&PurchaseAmount={string.Format("{0:0.00}", paymentStartDto.paymentTotal).Replace(",", ".")}&Currency=949&BrandName={(paymentStartDto.pan.StartsWith('4') ? "100" : "200")}&VerifyEnrollmentRequestId={oid}&SessionInfo={paymentStartDto.cv}&MerchantID={VakifbankHelper.merchantId}&MerchantPassword={VakifbankHelper.password}&SuccessUrl={okUrl}&FailureUrl={failUrl}&InstallmentCount={installment}"; //replace <value>

                var resultRequest = PaymentRequest.sendRequest(new PostBankDto
                {
                    path = paymentStartDto.path,
                    postData = post,
                    postUrl = _3dSecureServerUrl
                });

                if (!resultRequest.result)
                    return resultRequest;

                if (string.IsNullOrEmpty(resultRequest.message))
                    return new MessageModelResponse { result = false, message = MessageHelper.SORRY_MESSAGE_ERROR };

                var resultDictionary = paymentStartResultXmlParser(resultRequest.message);
                if (resultDictionary.ContainsKey("Status"))
                {
                    if (resultDictionary["Status"] == "Y" || resultDictionary["Status"] == "N")
                    {
                        return new MessageModelResponse
                        {
                            result = true,
                            message = $@"<html>  
                <head>
                <title>GET724 MPI 3D-Secure Processing Page</title>
                </head>
                <body>
                <form name='downloadForm' action='{resultDictionary["ACSUrl"]}' method='POST'>   <noscript>   <br>   <br>   <center>   <h1>Processing your 3-D Secure Transaction</h1>   <h2>   JavaScript is currently disabled or is not supported   by your browser.<br></h2>   <h3>Please click Submit to continue   the processing of your 3-D Secure 
                  transaction. Enrolled Returns true    </h3>   <input type='submit' value='Submit'>   </center>   </noscript>   <input type='hidden' name='PaReq' value='{resultDictionary["PaReq"]}'>   <input type='hidden' name='TermUrl' value='{resultDictionary["TermUrl"]}'>   <input type='hidden' name='MD' value='{resultDictionary["MerchantData"]}'>   </form>  <SCRIPT LANGUAGE='Javascript'>    document.downloadForm.submit();  </SCRIPT>  </body> </html>"
                        };
                    }
                    else
                    {
                        Log.log($"{paymentStartDto.path}\\logbank\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{paymentStartDto.userId}", resultRequest.message);
                        return new MessageModelResponse { result = false, message = MessageHelper.SORRY_PAYMENTSTART_MESSAGE_ERROR };
                    }
                }
                else
                {
                    Log.log($"{paymentStartDto.path}\\logbank\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{paymentStartDto.userId}", resultRequest.message);
                    return new MessageModelResponse { result = false, message = MessageHelper.SORRY_MESSAGE_ERROR };
                }
                
            }
            catch (Exception ex)
            {
                Log.log($"{paymentStartDto.path}\\logbank\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{paymentStartDto.userId}", ex.Message);
                return new MessageModelResponse { result = false, message = MessageHelper.SORRY_MESSAGE_ERROR };
            }
        }

        private Dictionary<string, string> paymentStartResultXmlParser(string xmlString)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(new StringReader(xmlString));

            //Status Bilgisi okunuyor
            XmlNode StatusNode = doc.GetElementsByTagName("Status").Item(0);
            String Status = "";
            if (StatusNode != null)
                Status = StatusNode.InnerText;

            //PAReq Bilgisi okunuyor
            XmlNode PAReqNode = doc.GetElementsByTagName("PaReq").Item(0);
            String PaReq = "";
            if (PAReqNode != null)
                PaReq = PAReqNode.InnerText;

            //ACSUrl Bilgisi okunuyor
            XmlNode ACSUrlNode = doc.GetElementsByTagName("ACSUrl").Item(0);
            String ACSUrl = "";
            if (ACSUrlNode != null)
                ACSUrl = ACSUrlNode.InnerText;

            //Term Url Bilgisi okunuyor
            XmlNode TermUrlNode = doc.GetElementsByTagName("TermUrl").Item(0);
            String TermUrl = "";
            if (TermUrlNode != null)
                TermUrl = TermUrlNode.InnerText;

            //MD Bilgisi okunuyor
            XmlNode MDNode = doc.GetElementsByTagName("MD").Item(0);
            String MD = "";
            if (MDNode != null)
                MD = MDNode.InnerText;

            // Sonuç dizisi olusturuluyor
            Dictionary<String, String> dic = new Dictionary<string, string>();
            dic.Add("Status", Status);
            dic.Add("PaReq", PaReq);
            dic.Add("ACSUrl", ACSUrl);
            dic.Add("TermUrl", TermUrl);
            dic.Add("MerchantData", MD);

            return dic;
        }
    }
}
