﻿using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Banks
{
    public interface IPaymentRequest
    {
        MessageModelResponse sendRequest(PostBankDto postBankDto);
    }
}
