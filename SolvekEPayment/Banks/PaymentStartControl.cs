﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models;

namespace SolvekEPayment.Banks
{
    public class PaymentStartControl : IPaymentInitControl
    {
        public MessageModelResponse control<T>(T value) where T : class
        {
            PaymentStartDto paymentStartDto = value as PaymentStartDto;
            if (string.IsNullOrEmpty(paymentStartDto.pan) || string.IsNullOrEmpty(paymentStartDto.cv)
                || string.IsNullOrEmpty(paymentStartDto.expire) || string.IsNullOrEmpty(paymentStartDto.holder))
                return new MessageModelResponse { result = false, message = MessageHelper.NOT_EMPTY };

            if (paymentStartDto.paymentTotal < 1)
                return new MessageModelResponse { result = false, message = MessageHelper.TOTAL_ERROR };

            if (paymentStartDto.userId <= 0)
                return new MessageModelResponse { result = false, message = MessageHelper.USER_ERROR };

            if (paymentStartDto.pan.Length != 19)
                return new MessageModelResponse { result = false, message = MessageHelper.PAN_ERROR };

            if (paymentStartDto.cv.Length != 3)
                return new MessageModelResponse { result = false, message = MessageHelper.CVV_ERROR };

            if (paymentStartDto.expire.Length != 5)
                return new MessageModelResponse { result = false, message = MessageHelper.EXPIRE_ERROR };

            if(paymentStartDto.installment < 0)
                return new MessageModelResponse { result = false, message = MessageHelper.INSTALLMENT_ERROR };

            if (paymentStartDto.bankId < 0)
                return new MessageModelResponse { result = false, message = MessageHelper.BANK_ERROR };

            if (paymentStartDto.cardId < 0)
                return new MessageModelResponse { result = false, message = MessageHelper.CARD_ERROR };

            return new MessageModelResponse { result = true, message = MessageHelper.OK };
        }
    }
}
