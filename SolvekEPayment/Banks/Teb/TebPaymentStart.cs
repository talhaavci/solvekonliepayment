﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models;
using SolvekEPayment.SolvekLog;

namespace SolvekEPayment.Banks.Teb
{
    public class TebPaymentStart : IPaymentStart
    {
        private readonly ILog Log;
        private readonly IPaymentRequest PaymentRequest;
        private readonly IConfiguration Configuration;
        string _3dSecureServerUrl = "https://sanalpos.teb.com.tr/fim/est3Dgate";
        //string _3dSecureServerUrl = "https://entegrasyon.asseco-see.com.tr/fim/est3Dgate";
        string rnd = DateTime.Now.ToString();
        string transactionType = "Auth";
        public TebPaymentStart(ILog log, IPaymentRequest paymentRequest, IConfiguration configuration)
        {
            PaymentRequest = paymentRequest;
            Log = log;
            Configuration = configuration;
        }
        public MessageModelResponse paymentStart<T>(T value) where T : class
        {
            PaymentStartDto paymentStartDto = value as PaymentStartDto;
            try
            {
                string oid = Guid.NewGuid().ToString();
                string okUrl = $"{(paymentStartDto.paymentType == PaymentTypeHelper.BALANCE ? Configuration.GetSection("paymentOkUrl").Value : Configuration.GetSection("paymentInvoiceOkUrl").Value)}?bType=other_{paymentStartDto.userId}_{paymentStartDto.bankId}_{paymentStartDto.cardId}";
                string failUrl = $"{(paymentStartDto.paymentType == PaymentTypeHelper.BALANCE ? Configuration.GetSection("paymentFailUrl").Value : Configuration.GetSection("paymentInvoiceFailUrl").Value)}?bType=other_{paymentStartDto.userId}_{paymentStartDto.bankId}_{paymentStartDto.cardId}";
                string installment = paymentStartDto.installment == 1 || paymentStartDto.installment == 0 ? "" : paymentStartDto.installment.ToString();
                //string hashstr = AkbankHelper.clientId + oid + string.Format("{0:0.00}", paymentStartDto.paymentTotal).Replace(",", ".")
                //+ okUrl + failUrl + transactionType + installment + rnd + AkbankHelper.storeKey;
                string[] expires = paymentStartDto.expire.Split('/');

                /*System.Security.Cryptography.SHA1 sha = new System.Security.Cryptography.SHA1CryptoServiceProvider();
                byte[] hashbytes = System.Text.Encoding.GetEncoding("ISO-8859-9").GetBytes(hashstr);
                byte[] inputbytes = sha.ComputeHash(hashbytes);

                string hash = Convert.ToBase64String(inputbytes);*/

                List<KeyValuePair<String, String>> postParams = new List<KeyValuePair<String, String>>();
                postParams.Add(new KeyValuePair<String, String>("pan", paymentStartDto.pan.Replace(" ", "").Trim()));
                postParams.Add(new KeyValuePair<String, String>("cv2", paymentStartDto.cv));
                postParams.Add(new KeyValuePair<String, String>("Ecom_Payment_Card_ExpDate_Year", expires[1].Trim()));
                postParams.Add(new KeyValuePair<String, String>("Ecom_Payment_Card_ExpDate_Month", Convert.ToInt16(expires[0].Trim()).ToString("00")));
                postParams.Add(new KeyValuePair<String, String>("cardType", (paymentStartDto.pan.StartsWith("4") ? "1" : "2")));
                postParams.Add(new KeyValuePair<String, String>("clientid", TebHelper.clientId));
                postParams.Add(new KeyValuePair<String, String>("amount", string.Format("{0:0.00}", paymentStartDto.paymentTotal).Replace(",", ".")));
                postParams.Add(new KeyValuePair<String, String>("okUrl", okUrl));
                postParams.Add(new KeyValuePair<String, String>("failUrl", failUrl));
                postParams.Add(new KeyValuePair<String, String>("islemtipi", "Auth"));
                postParams.Add(new KeyValuePair<String, String>("taksit", installment));
                postParams.Add(new KeyValuePair<String, String>("currency", "949"));
                postParams.Add(new KeyValuePair<String, String>("rnd", rnd));
                postParams.Add(new KeyValuePair<String, String>("storetype", "3d_pay"));
                postParams.Add(new KeyValuePair<String, String>("lang", "tr"));
                postParams.Add(new KeyValuePair<String, String>("oid", oid));

                postParams.Add(new KeyValuePair<String, String>("firmaadi", paymentStartDto.userId.ToString()));
                postParams.Add(new KeyValuePair<String, String>("Faturafirma", paymentStartDto.documentNumber));
                postParams.Add(new KeyValuePair<String, String>("Fpostakodu", paymentStartDto.bankId.ToString()));
                postParams.Add(new KeyValuePair<String, String>("Fulkekodu", paymentStartDto.cardId.ToString()));

                postParams.Add(new KeyValuePair<String, String>("hashAlgorithm", "ver3"));

                postParams.Sort(
               delegate (KeyValuePair<string, string> firstPair,
               KeyValuePair<string, string> nextPair)
               {
                   return firstPair.Key.ToLower(new System.Globalization.CultureInfo("en-US", false)).CompareTo(nextPair.Key.ToLower(new System.Globalization.CultureInfo("en-US", false)));
               });


                string hashStr = "";

                foreach (KeyValuePair<String, String> pair in postParams)
                {
                    String escapedValue = pair.Value.Replace("\\", "\\\\").Replace("|", "\\|");
                    String lowerValue = pair.Key.ToLower(new System.Globalization.CultureInfo("en-US", false));
                    if (!"encoding".Equals(lowerValue) && !"hash".Equals(lowerValue) && !"countdown".Equals(lowerValue))
                    {
                        hashStr += escapedValue + "|";
                    }
                }
                hashStr += TebHelper.storeKey;

                System.Security.Cryptography.SHA512 sha = new System.Security.Cryptography.SHA512CryptoServiceProvider();
                byte[] hashbytes = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(hashStr);
                byte[] inputbytes = sha.ComputeHash(hashbytes);
                String hash = System.Convert.ToBase64String(inputbytes);

                string post = "pan=" + paymentStartDto.pan + "&cv2=" + paymentStartDto.cv
                            + $"&Ecom_Payment_Card_ExpDate_Year={expires[1]}"
                            + $"&Ecom_Payment_Card_ExpDate_Month={Convert.ToInt32(expires[0]).ToString("00")}"
                            + $"&cardType={(paymentStartDto.pan.StartsWith('4') ? "1" : "2")}"
                            + "&amount=" + string.Format("{0:0.00}", paymentStartDto.paymentTotal).Replace(",", ".")
                            + "&oid=" + oid + "&rnd=" + rnd + "&clientid=" + TebHelper.clientId
                            + "&okUrl=" + okUrl + "&failUrl=" + failUrl + "&hash=" + hash
                            + "&islemtipi=Auth&taksit=" + installment
                            + "&storetype=3d_pay&currency=" + 949 + "&lang=tr&firmaadi=" + paymentStartDto.userId
                            + "&Faturafirma=" + paymentStartDto.documentNumber + "&Fpostakodu=" + paymentStartDto.bankId
                            + "&Fulkekodu=" + paymentStartDto.cardId + "&hashAlgorithm=ver3";

                var resultRequest = PaymentRequest.sendRequest(new PostBankDto
                {
                    path = paymentStartDto.path,
                    postData = post,
                    postUrl = _3dSecureServerUrl
                });

                if (!resultRequest.result)
                {
                    return resultRequest;
                }
                else
                {
                    if (string.IsNullOrEmpty(resultRequest.message) || resultRequest.message.Contains("ErrMsg")
                    || resultRequest.message.Contains("mdErrorMsg"))
                    {
                        Log.log($"{paymentStartDto.path}\\logbank\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{paymentStartDto.userId}", resultRequest.message);
                        return new MessageModelResponse { result = false, message = MessageHelper.SORRY_PAYMENTSTART_MESSAGE_ERROR };
                    }
                }

                //List<KeyValuePair<String, String>> keyValuePairs = new List<KeyValuePair<string, string>>();
                HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                document.LoadHtml(resultRequest.message);
                //nodes = document.DocumentNode.SelectNodes("//form/input");
                var form = document.DocumentNode.SelectSingleNode("//form");

                // Find all input elements within the form
                var inputs = form.SelectNodes(".//input[@type='hidden']");

                string responseHtmlStr = $@"<html>  
                <head>
                <title>GET724 MPI 3D-Secure Processing Page</title>
				<meta http-equiv='Content-Language' content='tr'>
				<meta http-equiv='Pragma' content='no-cache'>
				<meta http-equiv='Expires' content='now'>
				<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>
				<meta name='Author' content='Uygulama Gelistirme Asseco SEE tarafindan yapilmistir'>
                </head>
                <body>
                <form name='downloadForm' action='https://goguvenliodeme.bkm.com.tr/troy/approve' method='POST'>  
				<noscript>   <br>   <br>   <center>   <h1>Devam etmek icin tiklayiniz.</h1>    <input type='submit' value='Submit'>  
				  </center>   </noscript>   <input type='hidden' name='goreq' [solvekValue]>  
				  </form>  <SCRIPT LANGUAGE='Javascript'> 
				  document.downloadForm.submit();  </SCRIPT>  </body> </html>";

                foreach (var item in inputs)
                {
                    try
                    {
                        if (item.Attributes["name"].Value == "goreq")
                        {
                            responseHtmlStr = responseHtmlStr.Replace("[solvekValue]", $"value='{item.Attributes["value"].Value}'");
                        }
                        //keyValuePairs.Add(new KeyValuePair<String, String>(item.Attributes["name"].Value, item.Attributes["value"].Value));
                    }
                    catch (Exception ex)
                    {

                    }
                }

                return new MessageModelResponse { result = true, message = responseHtmlStr };
            }
            catch (Exception ex)
            {
                Log.log($"{paymentStartDto.path}\\logbank\\{DateTime.Now.ToString("yyyy_MM_dd_HHmmssFFF")}_{paymentStartDto.userId}", ex.Message);
                return new MessageModelResponse { result = false, message = MessageHelper.SORRY_MESSAGE_ERROR };
            }
        }
    }
}
