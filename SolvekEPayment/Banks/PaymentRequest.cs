﻿using SolvekEPayment.Dtos.AppDtos;
using SolvekEPayment.Helpers;
using SolvekEPayment.Models;
using SolvekEPayment.SolvekLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SolvekEPayment.Banks
{
    public class PaymentRequest : IPaymentRequest
    {
        private readonly ILog Log;
        public PaymentRequest(ILog log)
        {
            Log = log;
        }

        public MessageModelResponse sendRequest(PostBankDto postBankDto)
        {
            try
            {
                ServicePointManager.Expect100Continue = true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                WebRequest _WebRequest = WebRequest.Create(postBankDto.postUrl);
                _WebRequest.Method = "POST";
                byte[] byteArray = Encoding.GetEncoding("ISO-8859-9").GetBytes(postBankDto.postData);
                _WebRequest.ContentType = "application/x-www-form-urlencoded";
                _WebRequest.ContentLength = byteArray.Length;

                Stream dataStream = _WebRequest.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                WebResponse _WebResponse = _WebRequest.GetResponse();
                dataStream = _WebResponse.GetResponseStream();

                StreamReader reader = new StreamReader(dataStream);
                return new MessageModelResponse { result = true, message = reader.ReadToEnd() };
            }
            catch (Exception ex)
            {
                Log.log(postBankDto.path, ex.Message);
                return new MessageModelResponse { result = false, message = MessageHelper.SORRY_MESSAGE_ERROR };
            }
        }
    }
}
