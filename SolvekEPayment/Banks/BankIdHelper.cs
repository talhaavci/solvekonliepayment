﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Banks
{
    public class BankIdHelper
    {
        public const int AKBANK = 1;
        public const int HALKBANK = 2;
        public const int TEB = 3;
        public const int ISBANK = 4;
        public const int VAKIFBANK = 5;
    }
}
