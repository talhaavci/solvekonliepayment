﻿using SolvekEPayment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Banks
{
    public interface IPaymentStart
    {
        MessageModelResponse paymentStart<T>(T value) where T : class;
    }
}
