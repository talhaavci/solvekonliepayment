﻿using SolvekEPayment.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.SolvekLog
{
    public class Log : ILog
    {
        public void log<T>(string path, T entity) where T : class
        {
            try
            {
                new XmlWrite<T>().Write(path, entity);
            }
            catch
            {

            }
        }
    }
}
