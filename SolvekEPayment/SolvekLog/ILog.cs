﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.SolvekLog
{
    public interface ILog
    {
        void log<T>(string path, T entity) where T : class;
    }
}
