﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models.AppModels
{
    public class InstallmentModelResponse
    {
        public int installment { get; set; }
    }
}
