﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models.AppModels
{
    public class LoginModelResponse : MessageModelResponse
    {
        public int userId { get; set; }
    }
}
