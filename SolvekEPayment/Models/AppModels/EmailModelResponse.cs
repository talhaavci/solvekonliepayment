﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models.AppModels
{
    public class EmailModelResponse
    {
        public string email { get; set; }
        public string password { get; set; }
        public string smtp { get; set; }
        public int port { get; set; }
    }
}
