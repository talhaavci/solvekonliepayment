﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models.AppModels
{
    public class ForgotModelResponse : MessageModelResponse
    {
        public string email { get; set; }
        public string customerName { get; set; }
        public string password { get; set; }
        public string userName { get; set; }
    }
}
