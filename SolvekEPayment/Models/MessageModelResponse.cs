﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models
{
    public class MessageModelResponse
    {
        public bool result { get; set; }
        public string message { get; set; }
    }
}
