﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models
{
    public class MainModelResponse<T> : MessageModelResponse
    {
        public T model { get; set; }
    }
}
