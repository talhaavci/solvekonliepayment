﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models
{
    public class PageViewModel<T> : MessageModelResponse
    {
        public List<T> data { get; set; }
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public double? netTotal { get; set; }
    }
}
