﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models.ErpModels
{
    public class CustomerBalanceTotalModelResponse
    {
        public double total { get; set; }
        public string curName { get; set; }
    }
}
