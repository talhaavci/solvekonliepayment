﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models.ErpModels
{
    public class InvoiceTitleModelResponse
    {
        public int id { get; set; }
        public string date { get; set; }
        public string documentNumber { get; set; }
        public double total { get; set; }
        public string curName { get; set; }
    }
}
