﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SolvekEPayment.Models.ErpModels
{
    public class CustomerBalanceModelResponse
    {
        public string date { get; set; }
        public string documentNumber { get; set; }
        public string type { get; set; }
        public double debit { get; set; }
        public double credit { get; set; }
        public double total { get; set; }
    }
}
